package aplicacao.controle;

import static aplicacao.controle.HomeController.home;
import aplicacao.model.DepartamentoModel;
import aplicacao.view.departamento.*;
import persistencia.dominio.Servico;
import persistencia.dominio.entidade.Departamento;

public class DepartamentoController {
    private static CadastroDepartamento cadastro;
    private static PrincipalDepartamento principalDepartamento;
    private static DeletarDepartamento deletar;
    
    public static void AbrirPrincipalDepartamento()
    {
        cadastro = new CadastroDepartamento();
        principalDepartamento   = new PrincipalDepartamento();
        deletar = new DeletarDepartamento();
        
        principalDepartamento.setVisible(true);
    }
    
    public static void AbrirCadastroDepartamento()
    {
        principalDepartamento.setVisible(false);
        cadastro.setVisible(true);
    }
    
    public static void AbrirDeletarDepartamento()
    {
        principalDepartamento.setVisible(false);
        deletar.setVisible(true);
    }
    
    public static void RetornarTelaPrincipalDepartamento()
    {
        deletar.setVisible(false);
        cadastro.setVisible(false);
        principalDepartamento.setVisible(true);
    }
    
    public static void VoltarHomeDepartamento()
    {
        principalDepartamento   = new PrincipalDepartamento();
        cadastro.setVisible(false);
        deletar.setVisible(false);
        principalDepartamento.setVisible(false);
        home.setVisible(true);
    }
    
    public static void CadastrarDepartamento(String nome, String local)
    {
        Departamento dep = new Departamento();
        dep.setNome(nome);
        dep.setLocal(local);
        
        Servico.Departamento().Salvar(dep);
        
        principalDepartamento   = new PrincipalDepartamento();
        principalDepartamento.setVisible(true);
        cadastro.setVisible(false);
    }
        
    public static void FiltrarDepartamento(String nome, String local)
    {
        DepartamentoModel.setDepartamentos(Servico.Departamento().Filtar(nome, local));
    }
    
    public static void DeletarDepartamento(long id)
    {
        if(id > 0)
            Servico.Departamento().Deletar(id);
    }
}
