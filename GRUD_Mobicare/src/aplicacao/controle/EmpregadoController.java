
package aplicacao.controle;

import static aplicacao.controle.HomeController.home;
import aplicacao.view.empregado.*;
import persistencia.dominio.Servico;
import persistencia.dominio.entidade.Departamento;
import persistencia.dominio.entidade.Empregado;

public class EmpregadoController {
    private static CadastroEmpregado cadastro;
    private static DeletarEmpregado deletar;
    public static PrincipalEmpregado principalEmpregado;
    
    public static void AbrirPrincipalEmpregado()
    {
        principalEmpregado  = new PrincipalEmpregado();
        deletar  = new DeletarEmpregado();
        cadastro  = new CadastroEmpregado();
        
        principalEmpregado.setVisible(true);
    }
    
    public static void AbrirCadastroEmpregado()
    {
        cadastro = new CadastroEmpregado();
        principalEmpregado.setVisible(false);
        cadastro.setVisible(true);
    }
    
    public static void AbrirDeletarEmpregado()
    {
        deletar = new DeletarEmpregado();
        principalEmpregado.setVisible(false);
        deletar.setVisible(true);
    }
    
    public static void FecharDeletarEmpregado()
    {
        principalEmpregado = new PrincipalEmpregado();
        principalEmpregado.setVisible(true);
        deletar.setVisible(false);
    }
   
    public static void VoltarHomeEmpregado()
    {
        principalEmpregado.setVisible(false);
        deletar.setVisible(false);
        cadastro.setVisible(false);
        home.setVisible(true);
    }
    
    public static void FecharCadastrarEmpregado()
    {
        principalEmpregado.setVisible(true);
        cadastro.setVisible(false);
    }
    
    
    public static void CadastrarEmpregado(String nome, String cpf, String email, long idDep)
    {
        Departamento dep = new Departamento();
        dep.setId(idDep);
        
        Empregado emp = new Empregado();
        emp.setNome(nome);
        emp.setEmail(email);
        emp.setCPF(cpf);
        emp.setDepartamento(dep);
        
        Servico.Empregado().Salvar(emp);
        
        principalEmpregado = new PrincipalEmpregado();
        principalEmpregado.setVisible(true);
        cadastro.setVisible(false);
    }
        
    public static void FiltrarEmpregado(String nome, String cpf)
    {
        Servico.Empregado().Filtar(nome, cpf);
    }
    
    public static void DeletarEmpregado(long id)
    {
        if(id > 0)
            Servico.Empregado().Deletar(id);
    }
}
