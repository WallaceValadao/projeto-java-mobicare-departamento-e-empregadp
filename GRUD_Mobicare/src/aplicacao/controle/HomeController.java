package aplicacao.controle;

import static aplicacao.controle.DepartamentoController.*;
import static aplicacao.controle.EmpregadoController.*;
import aplicacao.view.home.Home;
import static persistencia.repositorio.GerenteSessao.*;

public class HomeController {

    public static Home home;
    
    public static void main(String[] args) {
        
        FabricaSessao();

        if(Sessao != null) {
            home = new Home();
            home.setVisible(true);
        }
    }
    
    public static void AbrirEmpregado()
    {
        home.setVisible(false);
        AbrirPrincipalEmpregado();
    }
    
    public static void AbrirDepartamento()
    {
        home.setVisible(false);
        AbrirPrincipalDepartamento();
    }
}
