package aplicacao.model;

import java.util.*;
import persistencia.dominio.entidade.Departamento;
import persistencia.dominio.entidade.Empregado;

public class EmpregadoModels {
    
    private static  List<Empregado> Empregados;
    private static  List<Departamento> Departamentos;
    
    public EmpregadoModels() {
        Empregados = new ArrayList<Empregado>();
        Departamentos = new ArrayList<Departamento>();
    }
    
    
    public static List<Empregado> getEmpregados() {
        return Empregados;
    }
    
    public static List<Departamento> getDepartamentos() {
        return Departamentos;
    }

    public static void setEmpregados(List<Empregado> aEmpregados) {
        Empregados = aEmpregados;
    }
    
    public static void setDepartamentos(List<Departamento> aDepartamentos) {
        Departamentos = aDepartamentos;
    }
}
