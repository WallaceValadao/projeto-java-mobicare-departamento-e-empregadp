
package aplicacao.model;

import java.util.*;
import persistencia.dominio.entidade.Departamento;

public class DepartamentoModel {    
    private long Id;
    private String Nome;
    private String Local;
    
    private static List<Departamento> Departamentos;
    
    public DepartamentoModel()
    {
        Departamentos = new ArrayList<Departamento>();
    }

    public long getId() 
    { return Id; } 
    
    public String getNome() 
    { return Nome; }
    
    public String getLocal() 
    { return Local; }
    
    public static List<Departamento> getDepartamentos() {
        return Departamentos;
    }

    
    public void setId(long Id) 
    { this.Id = Id; }
    
    public void setNome(String Nome) 
    { this.Nome = Nome; }
    
    public void setLocal(String Local) 
    { this.Local = Local; }
    
    public static void setDepartamentos(List<Departamento> aDepartamentos) {
        Departamentos = aDepartamentos;
    }
}
