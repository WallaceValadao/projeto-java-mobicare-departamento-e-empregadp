package persistencia.dominio;

import persistencia.repositorio.*;

public class Servico {
    
    public static DepartamentoRepositorio Departamento()
    { return new DepartamentoRepositorio(); }
    
    public static EmpregadoRepositorio Empregado()
    { return new EmpregadoRepositorio(); }
}
