
package persistencia.dominio.entidade;

import java.util.List;

public class Departamento {
    public static final String TABELA = "T_DEPARTAMENTO";
    public static final String ID = "ID_DEPARTAMENTO";
    public static final String NOME = "DSC_NOME";
    public static final String LOCAL = "DSC_LOCAL";
    
    
    private long Id;
    private String Nome;
    private String Local;
    private List<Empregado> Empregados;

    //region Gets
    public long getId() {
        return Id;
    }

    public String getNome() {
        return Nome;
    }

    public String getLocal() {
        return Local;
    }

    public List<Empregado> getEmpregados() {
        return Empregados;
    }
    //endregion

    //region Sets
    public void setId(long Id) {
        this.Id = Id;
    }
    
    public void setNome(String Nome) {
        this.Nome = Nome;
    }
    
    public void setLocal(String Local) {
        this.Local = Local;
    }
    
    public void setEmpregados(List<Empregado> Empregados) {
        this.Empregados = Empregados;
    }
    //endregion
}
