package persistencia.dominio.entidade;

public class Empregado {
    public static final String TABELA = "T_EMPREGADO";
    public static final String ID = "ID_EMPREGADO";
    public static final String NOME =  "DSC_NOME";
    public static final String CPF = "DSC_CPF";
    public static final String EMAIL = "DSC_EMAIL";
    public static final String DEPARTAMENTO = "ID_DEPARTAMENTO";
    
    private long    Id;
    private String  Nome;
    private int     cpf;
    private String  Email;
    
    private Departamento Departamento;

    //region Gets
    public long getId() 
    { return Id; }
    
    public String getNome() 
    { return Nome; }
    
    public int getCPF() 
    { return cpf; }
    
    public String getEmail() 
    { return Email; }
    
    public Departamento getDepartamento() 
    { return Departamento; }
    //endregion
    
    
    //region Sets    
    public void setId(long Id) 
    { this.Id = Id; }
    
    public void setNome(String Nome) 
    { this.Nome = Nome; }
    
    public void setCPF(String CPF) 
    { 
        String cpfValido = "";
        for (int i = 0; i < CPF.length(); i++) {
 	  if(Character.isDigit(CPF.charAt(i))== true){
              cpfValido += CPF.charAt(i);
          }
 	}
        if(cpfValido != "")
            this.cpf = Integer.parseInt(cpfValido); 
    }
    
    public void setEmail(String Email) 
    { this.Email = Email; }
    
    public void setDepartamento(Departamento Departamento) 
    { this.Departamento = Departamento; }
    //endregion
}
