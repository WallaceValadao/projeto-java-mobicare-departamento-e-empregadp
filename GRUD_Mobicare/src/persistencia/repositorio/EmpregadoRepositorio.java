package persistencia.repositorio;

import com.mysql.jdbc.StringUtils;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import javax.swing.JOptionPane;
import persistencia.dominio.Servico;
import persistencia.dominio.entidade.Empregado;
import static persistencia.repositorio.GerenteSessao.*;

public class EmpregadoRepositorio {
    private Empregado empregado = new Empregado();

    public EmpregadoRepositorio() {
       FabricaSessao();
    }
    
    public List<Empregado> Filtar(String nome, String cpf)
    {
        List<Empregado> emps = new ArrayList<Empregado>();
        
        String qOver =  "SELECT * FROM " + empregado.TABELA + " ";
                
        if(!StringUtils.isNullOrEmpty(nome) && !StringUtils.isNullOrEmpty(cpf))
        {
            qOver += "where " + empregado.NOME + " LIKE '%" + nome + "%' "
                        + "and " + empregado.CPF + " LIKE '%" + cpf + "%' ";
        }
        else if(!StringUtils.isNullOrEmpty(nome) || !StringUtils.isNullOrEmpty(cpf))
        {
            qOver += "where ";
            
            if(!StringUtils.isNullOrEmpty(nome))
                qOver += empregado.NOME + " LIKE '%" + nome + "%' ";
            else
                qOver += empregado.CPF + " LIKE '%" + cpf + "%' ";
        }
        
       try{
           Sessao.executeQuery(qOver);
           ResultSet rs = Sessao.getResultSet();
           
           while (rs.next()) {
                
                Empregado emp = new Empregado();
                
                emp.setId(rs.getInt(emp.ID));
                emp.setNome(rs.getString(emp.NOME));
                emp.setCPF(rs.getString(emp.CPF));
                emp.setEmail(rs.getString(emp.EMAIL));
                emp.setDepartamento(Servico.Departamento()
                                        .BuscarID(rs.getLong(emp.DEPARTAMENTO)));
                //TODO Add departamento
                emps.add(emp);
            }
           //Desconectar();
       }
       catch (SQLException sqle)
       {
            JOptionPane.showMessageDialog(null, "Não foi possivel buscar os registros! " + sqle);
            System.exit(0);
       }
        
       return emps;
    }
        
    public void Salvar(Empregado emp)
    {
        try {
            FabricaSessao();
            
            String qOver = "";
            
            if(emp.getId() == 0)
                qOver = "INSERT INTO " + empregado.TABELA
                           + " (" + empregado.NOME + ", " + empregado.CPF + ", "
                           + empregado.EMAIL 
                           + (emp.getDepartamento() == null ? "" : (", " + empregado.DEPARTAMENTO))
                           + ") VALUES ('"
                           + emp.getNome() + "', '" + emp.getCPF()  + "', '"
                           + emp.getEmail()  + "', '" 
                           +  (emp.getDepartamento() == null ? "" : emp.getDepartamento().getId())
                           + "')";
            else
                qOver = "UPDATE "  + empregado.TABELA + "SET " + empregado.NOME
                        + " = '" + emp.getNome() + "', " + empregado.CPF + " = '"
                        + emp.getCPF()  + "', " + empregado.EMAIL + " = '"
                        + emp.getEmail()  + "', " + empregado.DEPARTAMENTO + " =' "
                        + emp.getDepartamento() + "' WHERE" + empregado.ID + " = " + emp.getId();
            
            
            Sessao.executeUpdate(qOver);
 
            //Desconectar();
            
       } catch (SQLException sqle)
       {
            JOptionPane.showMessageDialog(null, "Não foi possivel salvar o registro! " + sqle);
            System.exit(0);
       }
    }
    
    public void Deletar(long id)
    {
        try {
            String qOver = "DELETE FROM " + empregado.TABELA + " where " + empregado.ID + " = " + id;
            
            Sessao.executeUpdate(qOver);
            
            //Desconectar();
        }  catch (SQLException sqle) {
            JOptionPane.showMessageDialog(null, "Não foi possivel excluir o registro selecionado! " + sqle);
            System.exit(0);
       }
    }
}
