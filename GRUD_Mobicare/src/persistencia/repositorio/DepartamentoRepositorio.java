package persistencia.repositorio;

import com.mysql.jdbc.StringUtils;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import javax.swing.JOptionPane;
import persistencia.dominio.entidade.*;
import static persistencia.repositorio.GerenteSessao.*;

public class DepartamentoRepositorio {
    
    private Departamento departamento = new Departamento();
    private Empregado empregado = new Empregado();
    
    public DepartamentoRepositorio() {
       FabricaSessao();
    }
  
    public List<Departamento> Filtar(String nome, String local)
    {
        List<Departamento> deps = new ArrayList<Departamento>();
        
        String qOver =  "SELECT * FROM " + departamento.TABELA + " ";
        
        if(!StringUtils.isNullOrEmpty(nome) && !StringUtils.isNullOrEmpty(local))
        {
            qOver += "where " + departamento.NOME + " LIKE '%" + nome + "%' "
                        + "and " + departamento.LOCAL + " LIKE '%" + local + "%' ";
        }
        else if(!StringUtils.isNullOrEmpty(nome) || !StringUtils.isNullOrEmpty(local))
        {
            qOver += "where ";
            
            if(!StringUtils.isNullOrEmpty(nome))
                qOver += departamento.NOME + " LIKE '%" + nome + "%' ";
            else
                qOver += departamento.LOCAL + " LIKE '%" + local + "%' ";
        }
        
        
       try{
           Sessao.executeQuery(qOver);
           ResultSet rs = Sessao.getResultSet();
           
           while (rs.next()) {
                
                Departamento dep = new Departamento();
                
                dep.setId(rs.getInt(dep.ID));
                dep.setNome(rs.getString(dep.NOME));
                dep.setLocal(rs.getString(dep.LOCAL));
                
                deps.add(dep);
            }
           
           //Desconectar();
       }
       catch (SQLException sqle)
       {
            JOptionPane.showMessageDialog(null, "Não foi possivel buscar os registros! " + sqle);
            System.exit(0);
       }
        
       return deps;
    }
    
    public void Salvar(Departamento dep)
    {
        try {
            FabricaSessao();
            
            String qOver = "";
            
            if(dep.getId() == 0)
                qOver = "INSERT INTO " + departamento.TABELA
                                + " (" + departamento.NOME + ", "
                                + departamento.LOCAL + ") VALUES ('"
                                + dep.getNome() + "', '" + dep.getLocal() + "')";
            else
                qOver = "UPDATE "  + departamento.TABELA + "SET " + departamento.NOME
                        + " = '" + dep.getNome() + "', " + departamento.LOCAL + " = '"
                        + dep.getLocal() + "' WHERE" + departamento.ID + " = " + dep.getId();
            
            
            Sessao.executeUpdate(qOver);
 
            //Desconectar();
            
       } catch (SQLException sqle)
       {
            JOptionPane.showMessageDialog(null, "Não foi possivel salvar o registro! " + sqle);
            System.exit(0);
       }
    }
    
    public void Deletar(long id)
    {
        try {
            String qOver = "DELETE FROM " + departamento.TABELA + " where " + departamento.ID + " = " + id;
            
            Sessao.executeUpdate(qOver);
            
            //Desconectar();
            
        }  catch (SQLException sqle) {
            JOptionPane.showMessageDialog(null, "Não foi possivel excluir o registro selecionado! " + sqle);
            System.exit(0);
       }
    }
    
    public long TotalDepartamentosSemEmpregado()
    {
        String qOver =  "SELECT count(*) as total FROM " + departamento.TABELA + " where " 
                                         + departamento.ID 
                                         + " not in ( SELECT " + empregado.DEPARTAMENTO 
                                         + " from " + empregado.TABELA + ")";
        
        try{
            
            ResultSet rs = Sessao.executeQuery(qOver);
            rs.next();
            //Desconectar();
            return rs.getLong("total");
        }
        catch (SQLException sqle)
        {
            
            JOptionPane.showMessageDialog(null, "Não foi possivel buscar os registros! " + sqle);
            System.exit(0);
        }
       
       return 0;
    }
    
    public Departamento BuscarID(long id)
    {
        if(id > 0)
        {
            
            String qOver =  "SELECT * FROM " + departamento.TABELA + " where " 
                                         + departamento.ID + " = " + id;
        
            try{
            Sessao.executeQuery(qOver);
            ResultSet rs = Sessao.getResultSet();
           
            rs.next(); 
            Departamento dep = new Departamento(); 
            
            dep.setId(rs.getInt(dep.ID));
            dep.setNome(rs.getString(dep.NOME));
            dep.setLocal(rs.getString(dep.LOCAL));
           
            //Desconectar();
            
            return dep;
            }
            catch (SQLException sqle)
            {
                JOptionPane.showMessageDialog(null, "Não foi possivel buscar os registros! " + sqle);
                System.exit(0);
            }
       }
       return null;
    }
}
