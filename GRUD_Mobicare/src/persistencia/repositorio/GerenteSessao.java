package persistencia.repositorio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class GerenteSessao {
  private static String local = "//localhost/";
  private static String banco = "cadastromobicare";
  private static String login = "root";
  private static String senha = "root";
  
  public static Connection conexao;
  public static Statement Sessao;
  
  private static void Conectar() {
    //System.out.println("Conectando ao banco...");
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conexao =  DriverManager.getConnection("jdbc:mysql:" + local + banco,login,senha);
            //JOptionPane.showMessageDialog(null, "Conectado com sucesso!");
        } catch (ClassNotFoundException ex) {
            System.out.println("Classe não encontrada, adicione o driver nas bibliotecas.");
            //Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException sqle) {
            JOptionPane.showMessageDialog(null, "Problema ao conectar!" + sqle);
            System.exit(0);
        }
    }
  
  public static void Desconectar()
  {
      try
      {
          if(Sessao != null)
            Sessao.close();
          
          if(conexao != null)
             conexao.close();
      }
      catch (SQLException sqle)
      {
          JOptionPane.showMessageDialog(null, "Problema ao desconectar!");
          System.exit(0);
      }
  }
  
  public static void FabricaSessao()
  {
      try
      {
          if(conexao == null)
            Conectar();
          
          Sessao = conexao.createStatement();
      }
      catch (SQLException sqle)
      {
          JOptionPane.showMessageDialog(null, "Problema ao criar a sessão! " + sqle);
          System.exit(0);
      }
      
  }
}
